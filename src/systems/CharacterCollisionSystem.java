package codes.brick.phaseshift.systems;

import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.PhaseshiftSound;
import codes.brick.phaseshift.PowerupEffect;
import codes.brick.phaseshift.components.ColorComponent;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.FlickerComponent;
import codes.brick.phaseshift.components.FlipComponent;
import codes.brick.phaseshift.components.HealthComponent;
import codes.brick.phaseshift.components.PositionComponent;
import codes.brick.phaseshift.components.SizeComponent;
import codes.brick.phaseshift.components.TeamComponent;
import codes.brick.phaseshift.enums.Animation;
import codes.brick.phaseshift.enums.Powerup;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.math.Rectangle;

public class CharacterCollisionSystem extends EntitySystem {
  private ImmutableArray<Entity> entities;

  private static final int REBOUND = 100;
  private static final int DAMAGE = 10;

  private final PhaseshiftSound hurtSound;

  /**
   * Create the system. Also sets up the sounds we use.
   *
   * @param manager Manager to retrieve sounds from
   */
  public CharacterCollisionSystem(AssetManager manager) {
    hurtSound = new PhaseshiftSound("hurt", manager);
  }

  @Override
  public void addedToEngine(Engine engine) {
    entities =
        engine.getEntitiesFor(
            Family.all(
                    HealthComponent.class,
                    PositionComponent.class,
                    SizeComponent.class,
                    TeamComponent.class)
                .exclude(DeadComponent.class)
                .get());
  }

  @Override
  public void update(float deltaTime) {
    // TODO: A potential (likely un-needed) optimization here is to cache the boxes at the beginning
    // so we don't re-generate rectangles for each entity
    for (int i = 0; i < entities.size() - 1; i++) {
      Entity e1 = entities.get(i);
      SizeComponent size1 = Mappers.size.get(e1);
      PositionComponent pos1 = Mappers.position.get(e1);
      TeamComponent team1 = Mappers.team.get(e1);
      FlipComponent flip1 = Mappers.flip.get(e1);
      Rectangle box1 = new Rectangle(pos1.x, pos1.y, size1.width, size1.height);
      for (int j = i + 1; j < entities.size(); j++) {
        Entity e2 = entities.get(j);
        SizeComponent size2 = Mappers.size.get(e2);
        PositionComponent pos2 = Mappers.position.get(e2);
        TeamComponent team2 = Mappers.team.get(e2);
        FlipComponent flip2 = Mappers.flip.get(e2);
        Rectangle box2 = new Rectangle(pos2.x, pos2.y, size2.width, size2.height);
        if (box1.overlaps(box2) && !TeamComponent.sameTeam(team1, team2)) {
          if (pos1.x < pos2.x) {
            pos1.x -= REBOUND;
            pos2.x += REBOUND;
            if (flip1.horizontalFlip && flip2.horizontalFlip) {
              // If p1 is facing left and p2 is facing left
              damage(e1, DAMAGE, hurtSound, e2);
            } else if (!flip1.horizontalFlip && !flip2.horizontalFlip) {
              // If p1 is facing right and p2 is facing right
              damage(e2, DAMAGE, hurtSound, e1);
            }

          } else if (pos1.x > pos2.x) {
            pos1.x += REBOUND;
            pos2.x -= REBOUND;
            if (flip1.horizontalFlip && flip2.horizontalFlip) {
              // If p1 is facing left and p2 is facing left
              damage(e2, DAMAGE, hurtSound, e1);
            } else if (!flip1.horizontalFlip && !flip2.horizontalFlip) {
              // If p1 is facing right and p2 is facing right
              damage(e1, DAMAGE, hurtSound, e2);
            }
          } else {
            if (flip1.horizontalFlip) {
              pos1.x -= REBOUND;
            } else {
              pos1.x += REBOUND;
            }
            if (flip2.horizontalFlip) {
              pos2.x -= REBOUND;
            } else {
              pos2.x += REBOUND;
            }
            // no damage if they are on top of each other exactly
          }
        }
      }
    }
  }

  /**
   * Damage a given entity (subtract health, play sound, flicker).
   *
   * @param target Entity to damage
   */
  static void damage(Entity target, int damage, PhaseshiftSound sound, Entity source) {
    for (PowerupEffect pe : Mappers.powerupEffect.get(target).activePowerupEffects) {
      if (pe.activePowerup == Powerup.INVINCIBILITY) {
        return;
      }
    }
    for (PowerupEffect pe : Mappers.powerupEffect.get(source).activePowerupEffects) {
      if (pe.activePowerup == Powerup.DOUBLE_DAMAGE) {
        damage *= 2;
      }
    }
    target.add(new FlickerComponent());
    target.add(new ColorComponent());
    target.getComponent(HealthComponent.class).health -= damage;
    sound.play();
    Mappers.animation.get(target).nextAnimation = Animation.HURT;
  }
}

package codes.brick.phaseshift.systems;

import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.PhaseshiftSound;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.HealthComponent;
import codes.brick.phaseshift.components.LivesComponent;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;

public class DeathSystem extends EntitySystem {
  private ImmutableArray<Entity> entities;

  private final PhaseshiftSound deathSound;

  /**
   * Create the system. Also sets up the sounds we use.
   *
   * @param manager Manager to retrieve sounds from
   */
  public DeathSystem(AssetManager manager) {
    deathSound = new PhaseshiftSound("death", manager);
  }

  @Override
  public void addedToEngine(Engine engine) {
    entities =
        engine.getEntitiesFor(Family.all(HealthComponent.class).exclude(DeadComponent.class).get());
  }

  @Override
  public void update(float deltaTime) {
    for (Entity entity : entities) {
      HealthComponent health = Mappers.health.get(entity);
      LivesComponent lives = Mappers.lives.get(entity);
      if (health.health <= 0) {
        deathSound.play();
        if (lives == null || --lives.lives == 0) {
          // If no lives left, they ain't dead, they gone forever
          this.getEngine().removeEntity(entity);
          continue;
        }
        entity.add(new DeadComponent());
      }
    }
  }
}

use crate::components;
use crate::JumpState;
use amethyst::ecs::{Entities, Join, ReadStorage, System, WriteStorage};
pub struct StaticCollision;
use crate::components::jump::MAX_JUMP_SPEED;

const GROUND_Y: f32 = 0.0;

impl<'s> System<'s> for StaticCollision {
   type SystemData = (
      WriteStorage<'s, components::Position>,
      ReadStorage<'s, components::Health>,
      ReadStorage<'s, components::Size>,
      ReadStorage<'s, components::Team>,
      ReadStorage<'s, components::Dead>,
      ReadStorage<'s, components::Solid>,
      WriteStorage<'s, components::Jump>,
      ReadStorage<'s, components::Health>,
      ReadStorage<'s, components::Size>,
      ReadStorage<'s, components::Dead>,
      Entities<'s>,
   );

   fn run(
      &mut self,
      (
         mut positions,
         healths,
         sizes,
         teams,
         deads,
         solids,
         mut jumps,
         static_healths,
         static_sizes,
         static_deads,
         entities,
      ): Self::SystemData,
   ) {
      // iterate over every healthy thing
      // health needs position, health, size, dead, team, jump
      // static needs  position, health, size, dead, solid
      // overlap: position, health, size, dead. These components must be manually managed to make rust lifetimes happy
      for (_, _, _, size, jump, entity) in (&healths, &teams, !&deads, &sizes, &mut jumps, &entities).join() {
         if positions.get(entity).is_none() || deads.get(entity).is_some() {
            continue; //oops, this isn't a healthy thing.
                      //Normally we'd use .join() to filter these out but we need to keep the lifetime present we can iterate over static things as well
         }
         {
            let pos = positions.get_mut(entity).unwrap();
            if jump.jump_state == JumpState::Standing && pos.y == GROUND_Y {
               continue;
            }
            if jump.jump_state == JumpState::Falling && pos.y <= GROUND_Y {
               pos.y = GROUND_Y;
               jump.jump_state = JumpState::Standing;
               jump.jump_speed = MAX_JUMP_SPEED;
               continue;
            }
         }
         let mut on_surface = false;
         let pos = positions.get(entity).unwrap();
         let mut new_y_val = pos.y;
         for (static_pos, _, static_size, _, _) in
            (&positions, !&static_healths, &static_sizes, !&static_deads, &solids).join()
         {
            //iterate over all static things
            if jump.jump_state == JumpState::Falling && ontop_surface(pos, static_pos, size, static_size) {
               new_y_val = static_pos.y + static_size.height;
               jump.jump_state = JumpState::Standing;
               jump.jump_speed = MAX_JUMP_SPEED;
               on_surface = true;
               break;
            } else if jump.jump_state == JumpState::Jumping && beneath_surface(pos, static_pos, size, static_size) {
               new_y_val = static_pos.y;
               jump.jump_state = JumpState::Falling;
               jump.jump_speed = 0.0;
               break;
            } else if jump.jump_state == JumpState::Standing && ontop_surface(pos, static_pos, size, static_size) {
               on_surface = true;
               break;
            }
         }
         positions.get_mut(entity).unwrap().y = new_y_val;
         if jump.jump_state == JumpState::Standing && !on_surface {
            jump.jump_state = JumpState::Falling;
            jump.jump_speed = 0.0;
         }
      }
   }
}

fn ontop_surface(
   player_pos: &components::Position,
   static_pos: &components::Position,
   player_size: &components::Size,
   static_size: &components::Size,
) -> bool {
   player_pos.y > (static_pos.y + static_size.height / 2.0)
      && player_pos.y <= (static_pos.y + static_size.height)
      && (player_pos.x + player_size.width) >= static_pos.x
      && player_pos.x <= (static_pos.x + static_size.width)
}

fn beneath_surface(
   player_pos: &components::Position,
   static_pos: &components::Position,
   player_size: &components::Size,
   static_size: &components::Size,
) -> bool {
   (player_pos.y + player_size.height) > static_pos.y
      && (player_pos.y + player_size.height) < (static_pos.y + static_size.height / 2.0)
      && (player_pos.x + player_size.height / 2.0) >= static_pos.x
      && (player_pos.x + player_size.height / 2.0) <= (static_pos.x + static_size.width)
}

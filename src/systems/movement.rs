use crate::components;
use crate::Direction;
use amethyst::core::timing::Time;
use amethyst::ecs::{Join, Read, ReadStorage, System, WriteStorage};

pub struct Movement;

impl<'s> System<'s> for Movement {
   type SystemData = (
      WriteStorage<'s, components::DeltaPosition>,
      ReadStorage<'s, components::Movable>,
      Read<'s, Time>,
   );

   fn run(&mut self, (mut delta_positions, movables, time): Self::SystemData) {
      for (delta_position, movable) in (&mut delta_positions, &movables).join() {
         match movable.move_direction {
            Some(Direction::Left) => {
               delta_position.delta_x -= movable.horizontal_speed * time.delta_seconds();
            }
            Some(Direction::Right) => {
               delta_position.delta_x += movable.horizontal_speed * time.delta_seconds();
            }
            None => {}
         }
      }
   }
}

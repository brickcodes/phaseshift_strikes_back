use crate::components::{self, Dead, Size};
use crate::Direction;
use amethyst::core::timing::Time;
use amethyst::ecs::{Join, Read, ReadStorage, System, WriteStorage};

pub struct HorizontalPatrol;

impl<'s> System<'s> for HorizontalPatrol {
   type SystemData = (
      WriteStorage<'s, components::Position>,
      ReadStorage<'s, Dead>,
      ReadStorage<'s, Size>,
      WriteStorage<'s, components::HorizontalPatrol>,
      Read<'s, Time>,
   );

   fn run(&mut self, (mut positions, deads, sizes, mut hpcs, time): Self::SystemData) {
      for (position, (), size, hpc) in (&mut positions, !&deads, &sizes, &mut hpcs).join() {
         if hpc.current_direction == Direction::Right {
            position.x += hpc.speed * time.delta_seconds();
            if (position.x + size.width) >= hpc.x_2 {
               hpc.current_direction = Direction::Left;
            }
         } else {
            position.x -= hpc.speed * time.delta_seconds();
            if position.x <= hpc.x_1 {
               hpc.current_direction = Direction::Right;
            }
         }
      }
   }
}

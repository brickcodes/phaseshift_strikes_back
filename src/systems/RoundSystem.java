package codes.brick.phaseshift.systems;

import codes.brick.phaseshift.Assets;
import codes.brick.phaseshift.Colors;
import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.components.ColorComponent;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.FadeComponent;
import codes.brick.phaseshift.components.FlickerComponent;
import codes.brick.phaseshift.components.HealthComponent;
import codes.brick.phaseshift.components.JumpComponent;
import codes.brick.phaseshift.components.LifetimeComponent;
import codes.brick.phaseshift.components.OverlayComponent;
import codes.brick.phaseshift.components.PhaseshiftComponent;
import codes.brick.phaseshift.components.PositionComponent;
import codes.brick.phaseshift.components.ProjectileComponent;
import codes.brick.phaseshift.components.SizeComponent;
import codes.brick.phaseshift.components.TeamComponent;
import codes.brick.phaseshift.components.TextureComponent;
import codes.brick.phaseshift.enums.Animation;
import codes.brick.phaseshift.enums.JumpState;
import codes.brick.phaseshift.enums.PhaseshiftScreen;
import codes.brick.phaseshift.screens.game.GameScreen;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import java.util.ArrayList;
import java.util.List;

public class RoundSystem extends EntitySystem {
  private ImmutableArray<Entity> entities;
  private ImmutableArray<Entity> deadThings;
  private ImmutableArray<Entity> projectiles;

  private static final float OVERLAY_DURATION = 2;
  private static final float STARTING_ALPHA = 0.6f;

  private final GameScreen screen;

  private final AssetManager manager;

  public RoundSystem(GameScreen screen, AssetManager manager) {
    this.screen = screen;
    this.manager = manager;
  }

  @Override
  public void addedToEngine(Engine engine) {
    entities =
        engine.getEntitiesFor(
            Family.all(HealthComponent.class, TeamComponent.class)
                .exclude(DeadComponent.class)
                .get());
    deadThings = engine.getEntitiesFor(Family.all(DeadComponent.class).get());
    projectiles = engine.getEntitiesFor(Family.all(ProjectileComponent.class).get());
  }

  @Override
  public void update(float deltaTime) {
    List<Integer> teamsLeft = new ArrayList<Integer>();
    for (Entity entity : entities) {
      int team = Mappers.team.get(entity).team;
      if (!teamsLeft.contains(team)) {
        teamsLeft.add(team);
      }
    }
    if (teamsLeft.size() == 1) {
      // one team wins
      createOverlay(Colors.getColorForTeam(teamsLeft.get(0)), OVERLAY_DURATION);
    } else if (teamsLeft.size() == 0) {
      // tie
      createOverlay(Colors.getDefaultColor(), OVERLAY_DURATION);
    } else {
      // round is still in progress
      return;
    }
    // Round is over
    if (deadThings.size() == 0) {
      if (teamsLeft.size() == 0) {
        // Tie
        screen.winningTeam = 0;
      } else {
        screen.winningTeam = teamsLeft.get(0);
      }
      screen.nextScreen = PhaseshiftScreen.END;
      return;
    }
    // Clean up all projectiles on screen
    for (Entity projectile : projectiles) {
      this.getEngine().removeEntity(projectile);
    }
    // Reset everything that was alive
    for (Entity entity : entities) {
      resetState(entity);
    }
    // If dead things have lives left, bring em back and go again.
    for (Entity entity : deadThings) {
      // Dead things will be removed when no more lives,
      // so anything dead here must have lives remaining
      // (lives are subtracted on death)
      entity.remove(DeadComponent.class);
      resetState(entity);
    }
  }

  private void resetState(Entity entity) {
    // TODO: maybe implement a reset-able thing in components
    if (Mappers.health.get(entity) != null) {
      Mappers.health.get(entity).health = Mappers.health.get(entity).maxHealth;
    }
    if (Mappers.position.get(entity) != null) {
      Mappers.position.get(entity).x = Mappers.position.get(entity).initialX;
      Mappers.position.get(entity).y = Mappers.position.get(entity).initialY;
    }
    if (Mappers.ability.get(entity) != null) {
      Mappers.ability.get(entity).ability1Triggered = false;
      Mappers.ability.get(entity).ability1Cooldown = 0;
      Mappers.ability.get(entity).ability2Triggered = false;
      Mappers.ability.get(entity).ability2Cooldown = 0;
    }
    if (Mappers.flip.get(entity) != null) {
      Mappers.flip.get(entity).horizontalFlip = Mappers.flip.get(entity).initialHorizontalFlip;
    }
    if (Mappers.color.get(entity) != null) {
      Mappers.color.get(entity).color = Mappers.color.get(entity).initialColor;
    }
    if (Mappers.flicker.get(entity) != null) {
      entity.remove(FlickerComponent.class);
    }
    if (Mappers.animation.get(entity) != null) {
      Mappers.animation.get(entity).nextAnimation = null;
      Mappers.animation.get(entity).currentAnimation = Animation.IDLE;
      Mappers.animation.get(entity).currentFrame = 0;
    }
    if (Mappers.jump.get(entity) != null) {
      Mappers.jump.get(entity).jumpSpeed = JumpComponent.MAX_JUMP_SPEED;
      Mappers.jump.get(entity).jumpState = JumpState.STANDING;
      Mappers.jump.get(entity).jumpTriggered = false;
    }
    if (Mappers.phaseshift.get(entity) != null) {
      entity.add(Mappers.phaseshift.get(entity).position);
      entity.remove(PhaseshiftComponent.class);
      Mappers.position.get(entity).x = Mappers.position.get(entity).initialX;
      Mappers.position.get(entity).y = Mappers.position.get(entity).initialY;
    }
    if (Mappers.powerupEffect.get(entity) != null) {
      Mappers.powerupEffect.get(entity).activePowerupEffects.clear();
    }
  }

  private Entity createOverlay(Color setColor, float duration) {
    Entity overlay = new Entity();
    LifetimeComponent lifetime = new LifetimeComponent(duration);
    overlay.add(lifetime);
    SizeComponent size = new SizeComponent(GameScreen.VIEWPORT_WIDTH, GameScreen.VIEWPORT_HEIGHT);
    overlay.add(size);
    overlay.add(new PositionComponent());
    ColorComponent color = new ColorComponent();
    color.initialColor = new Color(setColor);
    color.initialColor.a = STARTING_ALPHA;
    color.color = new Color(color.initialColor);
    overlay.add(color);
    overlay.add(new FadeComponent());
    overlay.add(
        new TextureComponent(manager.get(Assets.lookupTexturePath("overlay"), Texture.class)));
    overlay.add(new OverlayComponent());
    getEngine().addEntity(overlay);
    return overlay;
  }
}

use crate::components;
use amethyst::ecs::{Entities, Join, ReadStorage, System, WriteStorage, Entity};

#[derive(Clone)]
struct PositionChange {
   pub x_orig: f32,
   pub y_orig: f32,
   pub delta_x: f32,
   pub delta_y: f32,
   pub width: f32,
   pub height: f32,
}

#[derive(Clone, Default)]
struct Rectangle {
   pub x: f32,
   pub y: f32,
   pub width: f32,
   pub height: f32,
}

impl Rectangle {
   fn overlaps(&self, other: &Rectangle) -> bool {
      self.x < other.x + self.width &&
         self.x + self.width > other.x &&
         self.y < other.y + other.height &&
         self.y + self.height > other.y
   }
}

impl PositionChange {
   fn rect(&self) -> Rectangle {
      let mut rect = Rectangle::default();
      if self.delta_x < 0.0 {
         rect.x = self.x_orig + self.delta_x;
      } else {
         rect.x = self.x_orig;
      }
      if self.delta_y < 0.0 {
         rect.y = self.y_orig + self.delta_y;
      } else {
         rect.y = self.y_orig;
      }
      rect.width = self.delta_x.abs() + self.width;
      rect.height = self.delta_y.abs() + self.height;
      rect
   }
}

pub struct PositionFinalize;

impl<'s> System<'s> for PositionFinalize {
   type SystemData = (
      Entities<'s>,
      WriteStorage<'s, components::DeltaPosition>,
      WriteStorage<'s, components::Position>,
      WriteStorage<'s, components::Health>,
      ReadStorage<'s, components::Size>,
      ReadStorage<'s, components::Projectile>,
      ReadStorage<'s, components::Solid>,
   );

   fn run(&mut self, (entities, mut delta_positions, mut positions, mut healths, sizes, projectiles, solids): Self::SystemData) {
      let mut moving_stuff: Vec<(Entity, PositionChange)> = Vec::new();
      for (entity, delta_position, size) in (&entities, &mut delta_positions, &sizes).join() {
         if let Some(position) = positions.get_mut(entity) {
            rects.push((entity, PositionChange {
               x_orig: position.x,
               y_orig: position.y,
               delta_x: delta_position.delta_x,
               delta_y: delta_position.delta_y,
               width: size.width,
               height: size.height,
            }))
         }
         delta_position.delta_x = 0.0;
         delta_position.delta_y = 0.0;
      }
      let healthy_things = rects.iter().filter(|(e, rp)| healths.get(*e).is_some());
      let solid_things = rects.iter().filter(|(e, rp)| solids.get(*e).is_some());
      let projectile_things = rects.iter().filter(|(e, rp)| projectiles.get(*e).is_some());
      // First, collide player vs. platform
      for healthy_thing in healthy_things {
         for solid in solid_things.clone() {
            if !healthy_thing.1.rect().overlaps(&solid.1.rect()) {
               continue;
            }

            // If we were coming 
         }
      }
      // Then, collide player vs. projectile
      // Then, collide player vs player
      // Finally, make sure everything end up in bounds and commit movement
      rects.iter().for_each(|(e, pc)| {
         let mut new_x = pc.x_orig + pc.delta_x;
         let new_y = pc.y_orig + pc.delta_y;
         // Out of bounds
         {
            let is_oob = if new_x < 0.0 {
               new_x = 0.0;
               true
            } else if (new_x + pc.width) > crate::ARENA_WIDTH {
               new_x = crate::ARENA_WIDTH - pc.width;
               true
            } else {
               false
            };

            if is_oob && projectiles.get(*e).is_some() {
               entities.delete(*e).unwrap();
               return;
            }
         }
         let position = positions.get_mut(*e).unwrap();
         position.x = new_x;
         position.y = new_y;
      })
   }
}

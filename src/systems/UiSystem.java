package codes.brick.phaseshift.systems;

import codes.brick.phaseshift.Colors;
import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.HealthComponent;
import codes.brick.phaseshift.components.PositionComponent;
import codes.brick.phaseshift.components.SizeComponent;
import codes.brick.phaseshift.components.TeamComponent;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class UiSystem extends EntitySystem {
  private ImmutableArray<Entity> entities;

  private final BitmapFont font;
  private final Batch batch;

  public UiSystem(AssetManager manager, SpriteBatch batch) {
    font = manager.get("PXSansRegular32.ttf");
    this.batch = batch;
  }

  @Override
  public void addedToEngine(Engine engine) {
    entities =
        engine.getEntitiesFor(
            Family.all(
                    HealthComponent.class,
                    TeamComponent.class,
                    PositionComponent.class,
                    SizeComponent.class)
                .exclude(DeadComponent.class)
                .get());
  }

  @Override
  public void update(float deltaTime) {
    batch.begin();
    for (Entity entity : entities) {
      font.setColor(Colors.getColorForTeam(Mappers.team.get(entity).team));
      font.draw(
          batch,
          String.valueOf(Mappers.health.get(entity).health),
          Mappers.position.get(entity).x,
          Mappers.position.get(entity).y + Mappers.size.get(entity).height + font.getCapHeight());
    }
    batch.end();
  }
}

package codes.brick.phaseshift.systems;

import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.components.ColorComponent;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.FlipComponent;
import codes.brick.phaseshift.components.OverlayComponent;
import codes.brick.phaseshift.components.PositionComponent;
import codes.brick.phaseshift.components.SizeComponent;
import codes.brick.phaseshift.components.TextureComponent;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class DrawSystem extends EntitySystem {
  private ImmutableArray<Entity> entities;
  private ImmutableArray<Entity> overlays;
  private final SpriteBatch batch;

  public DrawSystem(SpriteBatch batch) {
    this.batch = batch;
  }

  @Override
  public void addedToEngine(Engine engine) {
    entities =
        engine.getEntitiesFor(
            Family.all(PositionComponent.class, TextureComponent.class, SizeComponent.class)
                .exclude(DeadComponent.class, OverlayComponent.class)
                .get());
    overlays =
        engine.getEntitiesFor(
            Family.all(
                    PositionComponent.class,
                    TextureComponent.class,
                    SizeComponent.class,
                    OverlayComponent.class)
                .exclude(DeadComponent.class)
                .get());
  }

  @Override
  public void update(float deltaTime) {
    batch.begin();
    for (Entity entity : entities) {
      draw(entity);
    }
    for (Entity overlay : overlays) {
      draw(overlay);
    }
    batch.end();
  }

  private void draw(Entity entity) {
    PositionComponent position = Mappers.position.get(entity);
    TextureComponent texture = Mappers.texture.get(entity);
    FlipComponent flipped = Mappers.flip.get(entity);
    ColorComponent color = Mappers.color.get(entity);
    boolean isFlipped = flipped != null && flipped.horizontalFlip;
    if (color != null) {
      batch.setColor(color.color);
    } else {
      batch.setColor(Color.WHITE);
    }
    SizeComponent size = Mappers.size.get(entity);
    batch.draw(
        texture.texture,
        position.x,
        position.y,
        size.width,
        size.height,
        texture.srcX,
        texture.srcY,
        texture.srcWidth,
        texture.srcHeight,
        isFlipped,
        false);
  }
}

use crate::{components, Assets, JumpState};
use amethyst::assets::AssetStorage;
use amethyst::audio::output::Output;
use amethyst::audio::Source;
use amethyst::core::timing::Time;
use amethyst::ecs::{Join, Read, ReadExpect, System, WriteStorage};

const JUMP_CHANGE: f32 = 7200.0;

pub struct Jump;

impl<'s> System<'s> for Jump {
   type SystemData = (
      WriteStorage<'s, components::DeltaPosition>,
      WriteStorage<'s, components::Jump>,
      Read<'s, Time>,
      Read<'s, AssetStorage<Source>>,
      ReadExpect<'s, Assets>,
      ReadExpect<'s, Output>,
   );

   fn run(&mut self, (mut delta_positions, mut jumps, time, storage, assets, output): Self::SystemData) {
      let delta = time.delta_seconds();
      for (delta_position, jump) in (&mut delta_positions, &mut jumps).join() {
         match jump.jump_state {
            // Explicit Euler integration
            JumpState::Jumping => {
               delta_position.delta_y += jump.jump_speed * delta;
               jump.jump_speed -= JUMP_CHANGE * delta;
               if jump.jump_speed <= 0.0 {
                  jump.jump_state = JumpState::Falling;
                  jump.jump_speed = 0.0;
               }
            }
            JumpState::Falling => {
               delta_position.delta_y -= jump.jump_speed * delta;
               jump.jump_speed += JUMP_CHANGE * delta;
            }
            JumpState::Standing => {
               if jump.jump_triggered {
                  output.play_once(storage.get(&assets.jump_sfx.0).unwrap(), assets.jump_sfx.1);
                  jump.jump_state = JumpState::Jumping;
               }
            }
         }
         jump.jump_triggered = false;
      }
   }
}

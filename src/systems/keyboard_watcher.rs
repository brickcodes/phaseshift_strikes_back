use crate::components;
use crate::Direction;
use amethyst::ecs::{Join, Read, ReadStorage, System, WriteStorage};
use amethyst::input::InputHandler;

#[derive(Default)]
pub struct KeyboardWatcher {
   p1_ability_1_acked: bool,
   p1_ability_2_acked: bool,
   p2_ability_1_acked: bool,
   p2_ability_2_acked: bool,
}

impl<'s> System<'s> for KeyboardWatcher {
   type SystemData = (
      WriteStorage<'s, components::Movable>,
      WriteStorage<'s, components::Jump>,
      WriteStorage<'s, components::Ability>,
      ReadStorage<'s, components::PlayerNumber>,
      Read<'s, InputHandler<String, String>>,
   );

   fn run(&mut self, (mut movables, mut jumps, mut abilities, player_numbers, input): Self::SystemData) {
      for (player_number, mut movable, mut jump, mut ability) in
         (&player_numbers, &mut movables, &mut jumps, &mut abilities).join()
      {
         match player_number.number {
            components::player_number::Number::One => {
               let left = input.action_is_down("p1_left");
               let right = input.action_is_down("p1_right");
               let new_direction = match (left, right) {
                  (Some(true), Some(true)) | (Some(false), Some(false)) => None,
                  (Some(true), Some(false)) => Some(Direction::Left),
                  (Some(false), Some(true)) => Some(Direction::Right),
                  (_, _) => panic!("InputHandler not registered successfully"),
               };
               movable.move_direction = new_direction;
               if input.action_is_down("p1_jump").unwrap() {
                  //if unwrap fails, InputHandler not registered successfully
                  jump.jump_triggered = true;
               }
               if input.action_is_down("p1_ability_1").unwrap() {
                  if !self.p1_ability_1_acked {
                     ability.ability_1_triggered = true;
                     self.p1_ability_1_acked = true;
                  }
               } else {
                  self.p1_ability_1_acked = false;
               }
               if input.action_is_down("p1_ability_2").unwrap() {
                  if !self.p1_ability_2_acked {
                     ability.ability_2_triggered = true;
                     self.p1_ability_2_acked = true;
                  }
               } else {
                  self.p1_ability_2_acked = false;
               }
            }
            components::player_number::Number::Two => {
               let left = input.action_is_down("p2_left");
               let right = input.action_is_down("p2_right");
               let new_direction = match (left, right) {
                  (Some(true), Some(true)) | (Some(false), Some(false)) => None,
                  (Some(true), Some(false)) => Some(Direction::Left),
                  (Some(false), Some(true)) => Some(Direction::Right),
                  (_, _) => panic!("InputHandler not registered successfully"),
               };
               movable.move_direction = new_direction;
               if input.action_is_down("p2_jump").unwrap() {
                  //if unwrap fails, InputHandler not registered successfully
                  jump.jump_triggered = true;
               }
               if input.action_is_down("p2_ability_1").unwrap() {
                  if !self.p2_ability_1_acked {
                     ability.ability_1_triggered = true;
                     self.p2_ability_1_acked = true;
                  }
               } else {
                  self.p2_ability_1_acked = false;
               }
               if input.action_is_down("p2_ability_2").unwrap() {
                  if !self.p2_ability_2_acked {
                     ability.ability_2_triggered = true;
                     self.p2_ability_2_acked = true;
                  }
               } else {
                  self.p2_ability_2_acked = false;
               }
            }
         }
      }
   }
}

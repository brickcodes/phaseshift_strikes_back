package codes.brick.phaseshift.systems;

import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.PhaseshiftSound;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.HealthComponent;
import codes.brick.phaseshift.components.PositionComponent;
import codes.brick.phaseshift.components.ProjectileComponent;
import codes.brick.phaseshift.components.SizeComponent;
import codes.brick.phaseshift.components.TeamComponent;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.math.Rectangle;

public class ProjectileCollisionSystem extends EntitySystem {
  private ImmutableArray<Entity> projectiles;
  private ImmutableArray<Entity> healthyThings;

  private final PhaseshiftSound hurtSound;

  /**
   * Create the system. Also sets up the sounds we use.
   *
   * @param manager Manager to retrieve sounds from
   */
  public ProjectileCollisionSystem(AssetManager manager) {
    hurtSound = new PhaseshiftSound("hurt", manager);
  }

  @Override
  public void addedToEngine(Engine engine) {
    projectiles =
        engine.getEntitiesFor(
            Family.all(
                    ProjectileComponent.class,
                    PositionComponent.class,
                    SizeComponent.class,
                    TeamComponent.class)
                .get());
    healthyThings =
        engine.getEntitiesFor(
            Family.all(
                    HealthComponent.class,
                    PositionComponent.class,
                    SizeComponent.class,
                    TeamComponent.class)
                .exclude(DeadComponent.class)
                .get());
  }

  @Override
  public void update(float deltaTime) {
    for (Entity projectile : projectiles) {
      SizeComponent projSize = Mappers.size.get(projectile);
      PositionComponent projPos = Mappers.position.get(projectile);
      ProjectileComponent pc = Mappers.projectile.get(projectile);
      TeamComponent projTeam = Mappers.team.get(projectile);
      Rectangle projBox = new Rectangle(projPos.x, projPos.y, projSize.width, projSize.height);
      for (Entity healthyThing : healthyThings) {
        SizeComponent healthSize = Mappers.size.get(healthyThing);
        PositionComponent healthPos = Mappers.position.get(healthyThing);
        TeamComponent healthTeam = Mappers.team.get(healthyThing);
        Rectangle healthyBox =
            new Rectangle(healthPos.x, healthPos.y, healthSize.width, healthSize.height);
        if (projBox.overlaps(healthyBox)
            && !pc.hasDamaged.contains(healthyThing)
            && !TeamComponent.sameTeam(healthTeam, projTeam)) {
          pc.hasDamaged.add(healthyThing);
          CharacterCollisionSystem.damage(healthyThing, pc.damage, hurtSound, pc.owner);
        }
      }
    }
  }
}

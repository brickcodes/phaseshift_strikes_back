use crate::components;
use amethyst::core::timing::Time;
use amethyst::ecs::{Join, Read, ReadStorage, System, WriteStorage};

pub struct Cooldown;

impl<'s> System<'s> for Cooldown {
   type SystemData = (
      WriteStorage<'s, components::Ability>,
      ReadStorage<'s, components::Dead>,
      Read<'s, Time>,
      // TODO
      //ReadStorage<'s, components::PowerupEffect>,
   );

   fn run(&mut self, (mut abilities, deads, time): Self::SystemData) {
      for (ability, _) in (&mut abilities, !&deads).join() {
         let sub_amount = time.delta_seconds();

         // TODO
         /*
         for (PowerupEffect pe : Mappers.powerupEffect.get(entity).activePowerupEffects) {
         if (pe.activePowerup == Powerup.HALF_COOLDOWN) {
            subAmount *= 2;
            // We don't break, in order to let them stack
         }
         } */

         if ability.ability_1_cooldown > 0.0 {
            ability.ability_1_cooldown -= sub_amount;
            if ability.ability_1_cooldown < 0.0 {
               ability.ability_1_cooldown = 0.0;
            }
         }

         if ability.ability_2_cooldown > 0.0 {
            ability.ability_2_cooldown -= sub_amount;
            if ability.ability_2_cooldown < 0.0 {
               ability.ability_2_cooldown = 0.0;
            }
         }
      }
   }
}

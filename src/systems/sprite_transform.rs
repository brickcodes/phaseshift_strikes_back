use crate::components;
use amethyst::assets::AssetStorage;
use amethyst::core::{GlobalTransform, Transform};
use amethyst::ecs::{Entities, Join, Read, ReadStorage, System, WriteStorage};
use amethyst::renderer::{Camera, SpriteRender, SpriteSheet, Texture, TextureHandle};

pub struct SpriteTransform;

impl<'s> System<'s> for SpriteTransform {
   type SystemData = (
      WriteStorage<'s, GlobalTransform>,
      WriteStorage<'s, Transform>,
      ReadStorage<'s, components::Position>,
      ReadStorage<'s, components::Size>,
      ReadStorage<'s, SpriteRender>,
      ReadStorage<'s, TextureHandle>,
      ReadStorage<'s, Camera>,
      Read<'s, AssetStorage<Texture>>,
      Read<'s, AssetStorage<SpriteSheet>>,
      Entities<'s>,
   );

   fn run(
      &mut self,
      (
         mut global_transforms,
         mut transforms,
         positions,
         sizes,
         sprites,
         textures,
         camera,
         texture_assets,
         sprite_sheet_assets,
         entities,
      ): Self::SystemData,
   ) {
      for (entity, _) in (&entities, !&camera).join() {
         if positions.get(entity).is_none() || sizes.get(entity).is_none() {
            transforms.remove(entity);
            global_transforms.remove(entity);
            continue;
         }

         let position = positions.get(entity).unwrap();
         let size = sizes.get(entity).unwrap();

         let native_size = if let Some(sprite_r) = sprites.get(entity) {
            let sprite_num = sprite_r.sprite_number;
            let sprite_sheet = sprite_sheet_assets.get(&sprite_r.sprite_sheet).unwrap();
            let sprite = &sprite_sheet.sprites[sprite_num];
            (sprite.width, sprite.height)
         } else if let Some(tex_handle) = textures.get(entity) {
            let tex = texture_assets.get(tex_handle).unwrap();
            (tex.size().0 as f32, tex.size().1 as f32)
         } else {
            transforms.remove(entity);
            global_transforms.remove(entity);
            continue;
         };

         let mut new_transform = Transform::default();
         new_transform.set_x(position.x + size.width / 2.0);
         new_transform.set_y(position.y + size.height / 2.0);
         new_transform.set_z(position.z);
         new_transform.set_scale(size.width / native_size.0, size.height / native_size.1, 1.0);
         //new_transform.set_scale(1.0, 1.0, 1.0);
         transforms.insert(entity, new_transform).unwrap();
      }
   }
}

use crate::components;
use amethyst::core::timing::Time;
use amethyst::ecs::{Join, Read, ReadStorage, System, WriteStorage};
use amethyst::renderer::SpriteRender;

const ANIMATION_INTERVAL: f32 = 0.06;

#[derive(Default)]
pub struct Animation {
   timer: f32,
}

impl<'s> System<'s> for Animation {
   type SystemData = (
      Read<'s, Time>,
      WriteStorage<'s, components::Animation>,
      WriteStorage<'s, SpriteRender>,
      ReadStorage<'s, components::Dead>,
   );

   fn run(&mut self, (time, mut animations, mut sprites, deads): Self::SystemData) {
      self.timer -= time.delta_seconds();
      while self.timer <= 0.0 {
         for (animation, sprite, _) in (&mut animations, &mut sprites, !&deads).join() {
            if let Some(anim) = animation.next_anim {
               animation.cur_anim = anim;
               animation.next_anim = None;
               animation.cur_frame = 0;
            }
            if animation.anims.get(&animation.cur_anim).is_none()
               || animation.cur_frame >= animation.anims.get(&animation.cur_anim).unwrap().len()
            {
               animation.cur_anim = components::animation::Anim::Idle;
               animation.cur_frame = 0;
               continue;
            }
            // Animation exists and we're in range
            sprite.sprite_number = animation.anims.get(&animation.cur_anim).unwrap()[animation.cur_frame];
            animation.cur_frame += 1;
         }
         self.timer += ANIMATION_INTERVAL;
      }
   }
}

package codes.brick.phaseshift.systems;

import codes.brick.phaseshift.Assets;
import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.PowerupEffect;
import codes.brick.phaseshift.components.ColorComponent;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.HealthComponent;
import codes.brick.phaseshift.components.LifetimeComponent;
import codes.brick.phaseshift.components.PositionComponent;
import codes.brick.phaseshift.components.PowerupComponent;
import codes.brick.phaseshift.components.PowerupEffectComponent;
import codes.brick.phaseshift.components.SizeComponent;
import codes.brick.phaseshift.components.TeamComponent;
import codes.brick.phaseshift.components.TextureComponent;
import codes.brick.phaseshift.enums.Powerup;
import codes.brick.phaseshift.screens.game.GameScreen;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import java.util.Random;

public class PowerupSystem extends EntitySystem {

  private final Random rand;
  private final AssetManager manager;

  // In seconds.
  private static final int SPAWN_INTERVAL_MIN = 10;
  private static final int SPAWN_INTERVAL_MAX = 25;

  // Size
  private static final int WIDTH = 16;
  private static final int HEIGHT = 16;

  private static final int LIFETIME_MIN = 5;
  private static final int LIFETIME_MAX = 15;

  private static final int DIST_TO_GROUND =
      PlatformSystem.DIST_TO_GROUND + PlatformSystem.HEIGHT + HEIGHT * 2;

  private float remainingToSpawn;

  // TODO: fade powerups in somehow? to reward skill / area control, perhaps draw hollow powerup
  // at 1/4 opacity or something for some amount of time before it spawns

  /**
   * Create a new powerup system, setting the timer to spawn powerups.
   *
   * @param rand Global random to use for values.
   * @param manager AssetManager to get powerup sprites.
   */
  public PowerupSystem(Random rand, AssetManager manager) {
    this.rand = rand;
    this.manager = manager;
    resetCountdown();
  }

  private ImmutableArray<Entity> powerups;
  private ImmutableArray<Entity> players;

  @Override
  public void addedToEngine(Engine engine) {
    powerups =
        engine.getEntitiesFor(
            Family.all(PositionComponent.class, SizeComponent.class, PowerupComponent.class).get());
    players =
        engine.getEntitiesFor(
            Family.all(
                    HealthComponent.class,
                    PositionComponent.class,
                    SizeComponent.class,
                    TeamComponent.class,
                    PowerupEffectComponent.class)
                .exclude(DeadComponent.class)
                .get());
  }

  @Override
  public void update(float deltaTime) {
    updateSpawn(deltaTime);
    for (Entity powerup : powerups) {
      SizeComponent psize = Mappers.size.get(powerup);
      PositionComponent ppos = Mappers.position.get(powerup);
      Rectangle pbox = new Rectangle(ppos.x, ppos.y, psize.width, psize.height);
      for (Entity player : players) {
        SizeComponent hsize = Mappers.size.get(player);
        PositionComponent hpos = Mappers.position.get(player);
        Rectangle hbox = new Rectangle(hpos.x, hpos.y, hsize.width, hsize.height);
        if (hbox.overlaps(pbox)) {
          PowerupComponent pc = Mappers.powerup.get(powerup);
          switch (pc.type) {
            case DOUBLE_DAMAGE:
              PowerupEffect dd = new PowerupEffect();
              dd.activePowerup = Powerup.DOUBLE_DAMAGE;
              dd.timeRemaining = 5;
              Mappers.powerupEffect.get(player).activePowerupEffects.add(dd);
              break;
            case HEALING:
              Mappers.health.get(player).heal(50);
              break;
            case INVINCIBILITY:
              PowerupEffect invincibility = new PowerupEffect();
              invincibility.activePowerup = Powerup.INVINCIBILITY;
              invincibility.timeRemaining = 5;
              Mappers.powerupEffect.get(player).activePowerupEffects.add(invincibility);
              break;
            case HALF_COOLDOWN:
              PowerupEffect hc = new PowerupEffect();
              hc.activePowerup = Powerup.HALF_COOLDOWN;
              hc.timeRemaining = 5;
              Mappers.powerupEffect.get(player).activePowerupEffects.add(hc);
              break;
            default:
              assert false;
          }
          getEngine().removeEntity(powerup);
          break;
        }
      }
    }
  }

  private void updateSpawn(float deltaTime) {
    remainingToSpawn -= deltaTime;
    if (remainingToSpawn > 0) {
      return;
    }
    // Spawn a random powerup
    {
      switch (rand.nextInt(4)) {
        case 0:
          makePowerup(Powerup.INVINCIBILITY);
          break;
        case 1:
          makePowerup(Powerup.DOUBLE_DAMAGE);
          break;
        case 2:
          makePowerup(Powerup.HEALING);
          break;
        case 3:
          makePowerup(Powerup.HALF_COOLDOWN);
          break;
        default:
          assert false;
      }
    }
    resetCountdown();
  }

  private Entity makePowerup(Powerup p) {
    Entity powerup = new Entity();
    powerup.add(new SizeComponent(WIDTH, HEIGHT));
    powerup.add(new LifetimeComponent(rand.nextInt(LIFETIME_MAX - LIFETIME_MIN) + LIFETIME_MIN));
    PositionComponent pc = new PositionComponent();
    pc.x = rand.nextInt(GameScreen.VIEWPORT_WIDTH - 200) + 100;
    pc.initialX = pc.x;
    pc.y = DIST_TO_GROUND;
    pc.initialY = pc.y;
    powerup.add(pc);
    powerup.add(
        new TextureComponent(manager.get(Assets.lookupTexturePath("overlay"), Texture.class)));
    ColorComponent color = new ColorComponent();
    switch (p) {
      case DOUBLE_DAMAGE:
        color.initialColor = new Color(1, 0, 0, 1);
        break;
      case HEALING:
        color.initialColor = new Color(0, 1, 0, 1);
        break;
      case INVINCIBILITY:
        color.initialColor = new Color(1, 1, 0, 1);
        break;
      case HALF_COOLDOWN:
        color.initialColor = new Color(0, 0, 1, 1);
        break;
      default:
        assert false;
    }
    PowerupComponent poc = new PowerupComponent();
    poc.type = p;
    powerup.add(poc);
    color.color = new Color(color.initialColor);
    powerup.add(color);
    getEngine().addEntity(powerup);
    return powerup;
  }

  private void resetCountdown() {
    remainingToSpawn = rand.nextInt(SPAWN_INTERVAL_MAX - SPAWN_INTERVAL_MIN) + SPAWN_INTERVAL_MIN;
  }
}

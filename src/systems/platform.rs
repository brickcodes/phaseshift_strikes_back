use crate::components;
use crate::{Assets, Rand};
use amethyst::core::timing::Time;
use amethyst::ecs::{Entities, Entity, Read, ReadExpect, Resources, System, SystemData, WriteExpect, WriteStorage};
use amethyst::renderer::{Rgba, TextureHandle};
// use rand::distributions::uniform::Uniform;
use rand::Rng;

// In seconds.
const PLATFORM_INTERVAL_MIN: f32 = 20.0;
const PLATFORM_INTERVAL_MAX: f32 = 40.0;

// Size
const WIDTH: f32 = 100.0;
const HEIGHT: f32 = 16.0;

const LIFETIME_MIN: f32 = 5.0;
const LIFETIME_MAX: f32 = 15.0;
// TODO: investigate const uniform creation, and do same for PLAT_INTERVAL
//const LIFETIME_RANGE: Uniform<f32> = Uniform::new_inclusive(5.0, 15.0);

const DIST_TO_GROUND: f32 = 125.0;

const MOV_PLATFORM_SPEED: f32 = 100.0;

#[derive(Default)]
pub struct Platform {
   spawn_timer: f32,
}

impl<'s> System<'s> for Platform {
   type SystemData = (
      Read<'s, Time>,
      WriteStorage<'s, components::Position>,
      WriteStorage<'s, components::Size>,
      WriteStorage<'s, Rgba>,
      WriteStorage<'s, TextureHandle>,
      WriteStorage<'s, components::Lifetime>,
      WriteStorage<'s, components::Solid>,
      WriteStorage<'s, components::HorizontalPatrol>,
      WriteExpect<'s, Rand>,
      ReadExpect<'s, Assets>,
      Entities<'s>,
   );

   fn setup(&mut self, res: &mut Resources) {
      Self::SystemData::setup(res);
      self.spawn_timer += res
         .fetch_mut::<Rand>()
         .gen_range(PLATFORM_INTERVAL_MIN, PLATFORM_INTERVAL_MAX);
   }

   fn run(
      &mut self,
      (
         time,
         mut positions,
         mut sizes,
         mut rgbas,
         mut textures,
         mut lifetimes,
         mut solids,
         mut horizontal_patrols,
         mut rand,
         assets,
         entities,
      ): Self::SystemData,
   ) {
      self.spawn_timer -= time.delta_seconds();
      if self.spawn_timer > 0.0 {
         return;
      }
      // Spawn a new platform in the middle, or 2 on the sides
      {
         let lifetime = rand.gen_range(LIFETIME_MIN, LIFETIME_MAX);
         match rand.gen_range(0u8, 3u8) {
            0 => {
               make_platform(
                  crate::ARENA_WIDTH / 2.0 - WIDTH / 2.0,
                  lifetime,
                  &entities,
                  &assets,
                  &mut positions,
                  &mut sizes,
                  &mut lifetimes,
                  &mut textures,
                  &mut solids,
                  &mut rgbas,
               );
            }
            1 => {
               make_platform(
                  WIDTH,
                  lifetime,
                  &entities,
                  &assets,
                  &mut positions,
                  &mut sizes,
                  &mut lifetimes,
                  &mut textures,
                  &mut solids,
                  &mut rgbas,
               );
               make_platform(
                  crate::ARENA_WIDTH - WIDTH * 2.0,
                  lifetime,
                  &entities,
                  &assets,
                  &mut positions,
                  &mut sizes,
                  &mut lifetimes,
                  &mut textures,
                  &mut solids,
                  &mut rgbas,
               );
            }
            2 => {
               let direction = if rand.gen::<bool>() {
                  crate::Direction::Left
               } else {
                  crate::Direction::Right
               };
               let hpc = components::HorizontalPatrol {
                  x_1: WIDTH,
                  x_2: crate::ARENA_WIDTH - WIDTH,
                  speed: MOV_PLATFORM_SPEED,
                  current_direction: direction,
               };
               let plat = make_platform(
                  crate::ARENA_WIDTH / 2.0 - WIDTH / 2.0,
                  lifetime,
                  &entities,
                  &assets,
                  &mut positions,
                  &mut sizes,
                  &mut lifetimes,
                  &mut textures,
                  &mut solids,
                  &mut rgbas,
               );
               horizontal_patrols.insert(plat, hpc).unwrap();
            }
            _ => unreachable!(),
         }
      }
      self.spawn_timer += rand.gen_range(PLATFORM_INTERVAL_MIN, PLATFORM_INTERVAL_MAX);
   }
}

#[allow(clippy::too_many_arguments)]
fn make_platform(
   x: f32,
   lifetime: f32,
   entities: &Entities,
   assets: &Assets,
   positions: &mut WriteStorage<components::Position>,
   sizes: &mut WriteStorage<components::Size>,
   lifetimes: &mut WriteStorage<components::Lifetime>,
   textures: &mut WriteStorage<TextureHandle>,
   solids: &mut WriteStorage<components::Solid>,
   rgbas: &mut WriteStorage<Rgba>,
) -> Entity {
   let position = components::Position {
      x,
      y: DIST_TO_GROUND,
      z: 7.0,
   };
   let size = components::Size {
      width: WIDTH,
      height: HEIGHT,
   };
   // TODO: initialX, initialY, initialColor
   entities
      .build_entity()
      .with(position, positions)
      .with(size, sizes)
      .with(components::Lifetime::new(lifetime), lifetimes)
      .with(assets.white_pixel.clone(), textures)
      .with(components::Solid, solids)
      .with(Rgba::RED, rgbas)
      .build()
}

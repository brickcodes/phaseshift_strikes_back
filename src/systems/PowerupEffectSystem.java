package codes.brick.phaseshift.systems;

import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.PowerupEffect;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.PowerupEffectComponent;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import java.util.Iterator;

public class PowerupEffectSystem extends EntitySystem {
  private ImmutableArray<Entity> players;

  @Override
  public void addedToEngine(Engine engine) {
    players =
        engine.getEntitiesFor(
            Family.all(PowerupEffectComponent.class).exclude(DeadComponent.class).get());
  }

  @Override
  public void update(float deltaTime) {
    for (Entity player : players) {
      Iterator<PowerupEffect> iter =
          Mappers.powerupEffect.get(player).activePowerupEffects.iterator();

      while (iter.hasNext()) {
        PowerupEffect pe = iter.next();

        pe.timeRemaining -= deltaTime;
        if (pe.timeRemaining <= 0) {
          iter.remove();
        }
      }
    }
  }
}

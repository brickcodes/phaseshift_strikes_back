package codes.brick.phaseshift.systems;

import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.components.ColorComponent;
import codes.brick.phaseshift.components.FadeComponent;
import codes.brick.phaseshift.components.LifetimeComponent;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;

public class FadeSystem extends EntitySystem {
  private ImmutableArray<Entity> entities;

  @Override
  public void addedToEngine(Engine engine) {
    entities =
        engine.getEntitiesFor(
            Family.all(LifetimeComponent.class, ColorComponent.class, FadeComponent.class).get());
  }

  @Override
  public void update(float deltaTime) {
    for (Entity entity : entities) {
      ColorComponent color = Mappers.color.get(entity);
      LifetimeComponent lifetime = Mappers.lifetime.get(entity);
      color.color.a = color.initialColor.a * (lifetime.lifetime / lifetime.initialLifetime);
    }
  }
}

use crate::components;
use crate::{Assets, Direction, JumpState};
use amethyst::assets::AssetStorage;
use amethyst::audio::output::Output;
use amethyst::audio::Source;
use amethyst::ecs::{Entities, Join, Read, ReadExpect, ReadStorage, System, WriteStorage};
use amethyst::renderer::{Flipped, Hidden, TextureHandle, Transparent};

const ABILITY_1_MAX_COOLDOWN: f32 = 3.0;
const ABILITY_2_MAX_COOLDOWN: f32 = 4.0;

pub struct Puck;

impl<'s> System<'s> for Puck {
   type SystemData = (
      WriteStorage<'s, Hidden>,
      WriteStorage<'s, components::Position>,
      WriteStorage<'s, components::DeltaPosition>,
      WriteStorage<'s, components::Ability>,
      WriteStorage<'s, components::puck::Puck>,
      WriteStorage<'s, components::Team>,
      WriteStorage<'s, TextureHandle>,
      WriteStorage<'s, components::Movable>,
      WriteStorage<'s, components::Jump>,
      WriteStorage<'s, components::Size>,
      ReadStorage<'s, Flipped>,
      ReadStorage<'s, components::Dead>,
      WriteStorage<'s, components::Projectile>,
      WriteStorage<'s, components::puck::Phaseshift>,
      Entities<'s>,
      ReadExpect<'s, Assets>,
      ReadExpect<'s, Output>,
      Read<'s, AssetStorage<Source>>,
      WriteStorage<'s, Transparent>,
      WriteStorage<'s, components::Animation>,
   );

   fn run(
      &mut self,
      (
         mut hiddens,
         mut positions,
         mut delta_positions,
         mut abilities,
         mut pucks,
         mut teams,
         mut textures,
         mut movables,
         mut jumps,
         mut sizes,
         flips,
         deads,
         mut projectiles,
         mut phaseshifts,
         entities,
         assets,
         output,
         sources,
         mut transparents,
         mut animations,
      ): Self::SystemData,
   ) {
      for (ability, puck, jump, flip, _, entity, animation) in (
         &mut abilities,
         &mut pucks,
         &mut jumps,
         &flips,
         !&deads,
         &entities,
         &mut animations,
      )
         .join()
      {
         // we're manually managing these components so we can make rust lifetimes happy
         if sizes.get(entity).is_none() || positions.get(entity).is_none() || teams.get(entity).is_none() {
            continue;
         }
         let team_num = teams.get(entity).unwrap().team;
         if ability.ability_1_triggered {
            if ability.ability_1_cooldown == 0.0 {
               let position = positions.get(entity).unwrap();
               let size = sizes.get(entity).unwrap();
               // Position
               let mut orb_position = components::Position::default();
               orb_position.y = position.y + size.height / 4.0;
               orb_position.z = 1.0;
               // Movement
               let direction = if *flip == Flipped::Horizontal {
                  orb_position.x = position.x;
                  Direction::Left
               } else {
                  orb_position.x = position.x + size.width;
                  Direction::Right
               };
               let orb_movable = components::Movable {
                  move_direction: Some(direction),
                  horizontal_speed: 1200.0,
               };
               // Texture
               let orb_sprite = if team_num == 0 {
                  assets.blue_orb.clone()
               } else {
                  assets.red_orb.clone()
               };
               // Size
               let orb_size = components::Size::square(32.0);
               // Projectile
               let projectile = components::Projectile::with_owner(entity);
               // Team
               let orb_team = components::Team { team: team_num };
               // Done
               let orb = entities
                  .build_entity()
                  .with(orb_position, &mut positions)
                  .with(components::DeltaPosition::default(), &mut delta_positions)
                  .with(orb_movable, &mut movables)
                  .with(orb_size, &mut sizes)
                  .with(orb_sprite, &mut textures)
                  .with(orb_team, &mut teams)
                  .with(projectile, &mut projectiles)
                  .with(Transparent, &mut transparents)
                  .build();
               puck.last_orb = Some(orb);
               ability.ability_1_cooldown = ABILITY_1_MAX_COOLDOWN;
               animation.next_anim = Some(components::animation::Anim::AbilityOne);
               output.play_once(sources.get(&assets.shoot_sfx.0).unwrap(), assets.shoot_sfx.1);
            } else if let Some(last_orb) = puck.last_orb {
               if entities.is_alive(last_orb) {
                  let (orb_x, orb_y) = {
                     let orb_position = positions.get(last_orb).unwrap();
                     (orb_position.x, orb_position.y)
                  };
                  let position = positions.get_mut(entity).unwrap();
                  let size = sizes.get(entity).unwrap();
                  position.x = orb_x;
                  position.y = orb_y - size.height / 4.0;
                  jump.jump_state = JumpState::Falling;
                  jump.jump_speed = 0.0;
                  entities.delete(last_orb).unwrap();
                  output.play_once(sources.get(&assets.teleport_sfx.0).unwrap(), assets.teleport_sfx.1);
               }
            }
            ability.ability_1_triggered = false;
         }

         if ability.ability_2_triggered {
            if ability.ability_2_cooldown == 0.0 {
               let psc = components::puck::Phaseshift {
                  timer: 1.0,
                  position: positions.get(entity).unwrap().clone(),
               };
               delta_positions.remove(entity);
               positions.remove(entity);
               phaseshifts.insert(entity, psc).unwrap();
               hiddens.insert(entity, Hidden).unwrap();
               ability.ability_2_cooldown = ABILITY_2_MAX_COOLDOWN;
               output.play_once(sources.get(&assets.phaseshift_sfx.0).unwrap(), assets.phaseshift_sfx.1);
            }
            ability.ability_2_triggered = false;
         }
      }
   }
}

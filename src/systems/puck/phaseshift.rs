use crate::components;
use amethyst::core::timing::Time;
use amethyst::ecs::{Entities, Join, Read, ReadStorage, System, WriteStorage};
use amethyst::renderer::Hidden;

pub struct Phaseshift;

impl<'s> System<'s> for Phaseshift {
   type SystemData = (
      Entities<'s>,
      Read<'s, Time>,
      WriteStorage<'s, components::puck::Phaseshift>,
      WriteStorage<'s, components::Ability>,
      WriteStorage<'s, components::Position>,
      WriteStorage<'s, components::DeltaPosition>,
      ReadStorage<'s, components::Dead>,
      WriteStorage<'s, Hidden>,
   );

   fn run(
      &mut self,
      (entities, time, mut phaseshifts, mut abilities, mut positions, mut delta_positions, deads, mut hiddens): Self::SystemData,
   ) {
      for (entity, ability, _) in (&entities, &mut abilities, !&deads).join() {
         if phaseshifts.get(entity).is_none() {
            continue;
         }

         let phaseshift = phaseshifts.get_mut(entity).unwrap();
         if phaseshift.timer > 0.0 {
            phaseshift.timer -= time.delta_seconds();
            ability.ability_1_triggered = false;
            ability.ability_2_triggered = false;
            continue;
         }

         positions.insert(entity, phaseshift.position.clone()).unwrap();
         delta_positions
            .insert(entity, components::DeltaPosition::default())
            .unwrap();
         phaseshifts.remove(entity);
         hiddens.remove(entity);
      }
   }
}

package codes.brick.phaseshift.systems.puck;

import codes.brick.phaseshift.Assets;
import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.PhaseshiftSound;
import codes.brick.phaseshift.components.AbilityComponent;
import codes.brick.phaseshift.components.AnimationComponent;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.FlipComponent;
import codes.brick.phaseshift.components.JumpComponent;
import codes.brick.phaseshift.components.MovableComponent;
import codes.brick.phaseshift.components.PhaseshiftComponent;
import codes.brick.phaseshift.components.PositionComponent;
import codes.brick.phaseshift.components.ProjectileComponent;
import codes.brick.phaseshift.components.SizeComponent;
import codes.brick.phaseshift.components.TeamComponent;
import codes.brick.phaseshift.components.TextureComponent;
import codes.brick.phaseshift.components.puck.PuckComponent;
import codes.brick.phaseshift.enums.Animation;
import codes.brick.phaseshift.enums.Direction;
import codes.brick.phaseshift.enums.JumpState;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

public class PuckSystem extends EntitySystem {
  private ImmutableArray<Entity> entities;

  private final PhaseshiftSound teleportSound;
  private final PhaseshiftSound shootSound;
  private final PhaseshiftSound phaseShiftSound;

  private final Texture blueOrb;
  private final Texture redOrb;

  /**
   * Create the system. Also sets up the sounds we use.
   *
   * @param manager Manager to retrieve sounds from
   */
  public PuckSystem(AssetManager manager) {
    teleportSound = new PhaseshiftSound("teleport", manager);
    shootSound = new PhaseshiftSound("shoot", manager);
    phaseShiftSound = new PhaseshiftSound("phaseshift", manager);
    blueOrb = manager.get(Assets.lookupTexturePath("blueorb"));
    redOrb = manager.get(Assets.lookupTexturePath("redorb"));
  }

  @Override
  public void addedToEngine(Engine engine) {
    entities =
        engine.getEntitiesFor(
            Family.all(
                    PuckComponent.class,
                    AbilityComponent.class,
                    PositionComponent.class,
                    TextureComponent.class,
                    AnimationComponent.class,
                    JumpComponent.class)
                .exclude(DeadComponent.class)
                .get());
  }

  @Override
  public void update(float deltaTime) {
    for (Entity entity : entities) {
      AbilityComponent abilities = Mappers.ability.get(entity);
      PositionComponent position = Mappers.position.get(entity);
      FlipComponent flip = Mappers.flip.get(entity);
      TeamComponent team = Mappers.team.get(entity);
      JumpComponent jump = Mappers.jump.get(entity);
      PuckComponent puck = Mappers.puck.get(entity);
      SizeComponent size = Mappers.size.get(entity);

      if (abilities.ability1Triggered) {
        if (abilities.ability1Cooldown == 0) {
          Entity orb = new Entity();
          // Position
          PositionComponent orbPosition = new PositionComponent();
          orbPosition.y = position.y + size.height / 4;
          // Movement
          MovableComponent orbMovable = new MovableComponent();
          if (flip.horizontalFlip) {
            orbPosition.x = position.x;
            orbMovable.moveDirection = Direction.LEFT;
          } else {
            orbPosition.x = position.x + size.width;
            orbMovable.moveDirection = Direction.RIGHT;
          }
          orb.add(orbPosition);
          orbMovable.horizontalSpeed = 1200;
          orb.add(orbMovable);
          // Texture
          TextureComponent orbSprite;
          if (team.team == 1) {
            orbSprite = new TextureComponent(blueOrb);
          } else {
            orbSprite = new TextureComponent(redOrb);
          }
          orb.add(orbSprite);
          // Size
          orb.add(new SizeComponent(32));
          // Projectile
          ProjectileComponent pc = new ProjectileComponent();
          pc.owner = entity;
          orb.add(pc);
          // Team
          TeamComponent orbTeam = new TeamComponent();
          orbTeam.team = team.team;
          orb.add(orbTeam);
          // Done
          puck.lastOrb = orb;
          getEngine().addEntity(orb);
          abilities.ability1Cooldown = abilities.ability1MaxCooldown;
          shootSound.play();
          Mappers.animation.get(entity).nextAnimation = Animation.ABILITY_ONE;
        } else if (getEngine().getEntities().contains(puck.lastOrb, true)) {
          PositionComponent orbPosition = Mappers.position.get(puck.lastOrb);
          SizeComponent orbSize = Mappers.size.get(puck.lastOrb);
          position.x = orbPosition.x;
          position.y = orbPosition.y - orbSize.height;
          jump.jumpState = JumpState.FALLING;
          jump.jumpSpeed = 0;
          getEngine().removeEntity(puck.lastOrb);
          teleportSound.play();
        }
        abilities.ability1Triggered = false;
      }

      if (abilities.ability2Triggered) {
        if (abilities.ability2Cooldown == 0) {
          PhaseshiftComponent psc = new PhaseshiftComponent();
          psc.position = position;
          entity.remove(PositionComponent.class);
          entity.add(psc);
          abilities.ability2Cooldown = abilities.ability2MaxCooldown;
          abilities.ability2Triggered = false;
          phaseShiftSound.play();
        }
        abilities.ability2Triggered = false;
      }
    }
  }
}

package codes.brick.phaseshift.systems.puck;

import codes.brick.phaseshift.Mappers;
import codes.brick.phaseshift.components.AbilityComponent;
import codes.brick.phaseshift.components.AiComponent;
import codes.brick.phaseshift.components.AnimationComponent;
import codes.brick.phaseshift.components.DeadComponent;
import codes.brick.phaseshift.components.HealthComponent;
import codes.brick.phaseshift.components.JumpComponent;
import codes.brick.phaseshift.components.MovableComponent;
import codes.brick.phaseshift.components.PositionComponent;
import codes.brick.phaseshift.components.ProjectileComponent;
import codes.brick.phaseshift.components.SizeComponent;
import codes.brick.phaseshift.components.TeamComponent;
import codes.brick.phaseshift.components.TextureComponent;
import codes.brick.phaseshift.components.puck.PuckComponent;
import codes.brick.phaseshift.enums.Direction;
import codes.brick.phaseshift.enums.JumpState;
import codes.brick.phaseshift.screens.game.GameScreen;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import java.util.Random;

public class PuckAiSystem extends EntitySystem {
  private ImmutableArray<Entity> entities;
  private ImmutableArray<Entity> projectiles;
  private ImmutableArray<Entity> healthyThings;

  private final Random rand;

  public PuckAiSystem(Random rand) {
    this.rand = rand;
  }

  @Override
  public void addedToEngine(Engine engine) {
    entities =
        engine.getEntitiesFor(
            Family.all(
                    PuckComponent.class,
                    AbilityComponent.class,
                    PositionComponent.class,
                    TextureComponent.class,
                    AnimationComponent.class,
                    JumpComponent.class,
                    SizeComponent.class,
                    AiComponent.class)
                .exclude(DeadComponent.class)
                .get());
    projectiles =
        engine.getEntitiesFor(
            Family.all(
                    ProjectileComponent.class,
                    PositionComponent.class,
                    SizeComponent.class,
                    TeamComponent.class)
                .get());
    healthyThings =
        engine.getEntitiesFor(
            Family.all(
                    HealthComponent.class,
                    PositionComponent.class,
                    SizeComponent.class,
                    TeamComponent.class)
                .exclude(DeadComponent.class)
                .get());
  }

  @Override
  public void update(float deltaTime) {
    for (Entity ai : entities) {
      PositionComponent aiPosition = Mappers.position.get(ai);
      AbilityComponent abilityComponent = Mappers.ability.get(ai);
      JumpComponent jumpComponent = Mappers.jump.get(ai);
      if (jumpComponent.jumpState != JumpState.STANDING) {
        jumpComponent.jumpTriggered = false;
      }
      MovableComponent movableComponent = Mappers.movable.get(ai);
      SizeComponent sizeComponent = Mappers.size.get(ai);
      Vector2 center = new Vector2();
      new Rectangle(aiPosition.x, aiPosition.y, sizeComponent.width, sizeComponent.height)
          .getCenter(center);
      int aiTeam = Mappers.team.get(ai).team;
      Mappers.ai.get(ai).decisionTimer += deltaTime;
      if (Mappers.ai.get(ai).decisionTimer >= 1) {
        int randomChoice = rand.nextInt(3);
        if (randomChoice == 0) {
          movableComponent.moveDirection = null;
        } else if (randomChoice == 1) {
          movableComponent.moveDirection = Direction.LEFT;
        } else {
          movableComponent.moveDirection = Direction.RIGHT;
        }
        Mappers.ai.get(ai).decisionTimer = rand.nextFloat();
      }
      if (aiPosition.x == 0 && movableComponent.moveDirection == Direction.LEFT) {
        movableComponent.moveDirection = null;
        Mappers.flip.get(ai).horizontalFlip = false;
      } else if (aiPosition.x == GameScreen.VIEWPORT_WIDTH - Mappers.size.get(ai).width
          && movableComponent.moveDirection == Direction.RIGHT) {
        movableComponent.moveDirection = null;
        Mappers.flip.get(ai).horizontalFlip = true;
      }
      for (Entity projectile : projectiles) {
        PositionComponent projectilePosition = Mappers.position.get(projectile);
        SizeComponent projectileSize = Mappers.size.get(projectile);
        Vector2 projectileCenter = new Vector2();
        new Rectangle(
                projectilePosition.x,
                projectilePosition.y,
                projectileSize.width,
                projectileSize.height)
            .getCenter(projectileCenter);
        int projectileTeam = Mappers.team.get(projectile).team;
        if (projectileTeam != aiTeam
            && center.dst(projectileCenter) < (projectileSize.width + sizeComponent.width) * 2) {
          if (abilityComponent.ability2Cooldown == 0) {
            abilityComponent.ability2Triggered = true;
          } else if (jumpComponent.jumpState == JumpState.STANDING) {
            jumpComponent.jumpTriggered = true;
          }
        }
      }
      for (Entity otherPlayer : healthyThings) {
        if (otherPlayer == ai
            || TeamComponent.sameTeam(Mappers.team.get(ai), Mappers.team.get(otherPlayer))) {
          continue;
        }
        PositionComponent playerPosition = Mappers.position.get(otherPlayer);
        SizeComponent playerSize = Mappers.size.get(otherPlayer);
        Vector2 projectileCenter = new Vector2();
        new Rectangle(playerPosition.x, playerPosition.y, playerSize.width, playerSize.height)
            .getCenter(projectileCenter);
        if (Vector2.dst(aiPosition.x, aiPosition.y, playerPosition.x, playerPosition.y)
            < (playerSize.width + sizeComponent.width)) {
          Mappers.flip.get(ai).horizontalFlip = playerPosition.x <= aiPosition.x;
          movableComponent.moveDirection = null;
        }
        if (abilityComponent.ability1Cooldown == 0) {
          Mappers.flip.get(ai).horizontalFlip = playerPosition.x <= aiPosition.x;
          abilityComponent.ability1Triggered = true;
        }
      }
    }
  }
}

use crate::components;
use amethyst::core::timing::Time;
use amethyst::ecs::{Entities, Join, Read, System, WriteStorage};
use amethyst::renderer::Rgba;

const FLICKER_SPEED: f32 = 0.03;

#[derive(Default)]
pub struct Flicker {
   timer: f32,
}

impl<'s> System<'s> for Flicker {
   type SystemData = (
      Read<'s, Time>,
      Entities<'s>,
      WriteStorage<'s, components::Flicker>,
      WriteStorage<'s, Rgba>,
   );

   fn run(&mut self, (time, entities, mut flickers, mut colors): Self::SystemData) {
      self.timer -= time.delta_seconds();
      while self.timer <= 0.0 {
         for (entity, color) in (&entities, &mut colors).join() {
            if flickers.get(entity).is_none() {
               continue;
            }
            let flicker = flickers.get_mut(entity).unwrap();
            flicker.flicker_time -= FLICKER_SPEED;
            if flicker.flicker_time <= 0.0 {
               *color = flicker.original_color;
               flickers.remove(entity);
               continue;
            }
            if *color == flicker.original_color {
               *color = Rgba::BLACK;
            } else {
               *color = flicker.original_color;
            }
         }
         self.timer += FLICKER_SPEED;
      }
   }
}

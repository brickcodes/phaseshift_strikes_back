use crate::components;
use amethyst::core::timing::Time;
use amethyst::ecs::{Entities, Join, Read, System, WriteStorage};

pub struct Lifetime;

impl<'s> System<'s> for Lifetime {
   type SystemData = (WriteStorage<'s, components::Lifetime>, Read<'s, Time>, Entities<'s>);

   fn run(&mut self, (mut lifetimes, time, entities): Self::SystemData) {
      for (entity, lifetime) in (&entities, &mut lifetimes).join() {
         lifetime.lifetime -= time.delta_seconds();
         if lifetime.lifetime <= 0.0 {
            entities.delete(entity).unwrap();
         }
      }
   }
}

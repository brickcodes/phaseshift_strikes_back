use crate::components;
use crate::Direction;
use amethyst::ecs::{Entities, Join, ReadStorage, System, WriteStorage};
use amethyst::renderer::Flipped;

pub struct Flip;

impl<'s> System<'s> for Flip {
   type SystemData = (
      ReadStorage<'s, components::Movable>,
      WriteStorage<'s, Flipped>,
      Entities<'s>,
   );

   fn run(&mut self, (movables, mut flippeds, entities): Self::SystemData) {
      for (movable, entity) in (&movables, &entities).join() {
         match movable.move_direction {
            Some(Direction::Left) => {
               flippeds.insert(entity, Flipped::Horizontal).unwrap();
            }
            Some(Direction::Right) => {
               flippeds.insert(entity, Flipped::None).unwrap();
            }
            None => {}
         }
      }
   }
}

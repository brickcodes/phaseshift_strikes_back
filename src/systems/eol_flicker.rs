use crate::components;
use amethyst::ecs::{Entities, Join, ReadStorage, System, WriteStorage};
use amethyst::renderer::Rgba;

const DURATION_FRACTION: f32 = 5.0;

pub struct EolFlicker;

impl<'s> System<'s> for EolFlicker {
   type SystemData = (
      Entities<'s>,
      WriteStorage<'s, components::Flicker>,
      ReadStorage<'s, components::Lifetime>,
      ReadStorage<'s, components::Overlay>,
      ReadStorage<'s, Rgba>,
   );

   fn run(&mut self, (entities, mut flickers, lifetimes, overlays, colors): Self::SystemData) {
      for (entity, lifetime, _) in (&entities, &lifetimes, !&overlays).join() {
         if flickers.get(entity).is_some() {
            continue;
         }

         if lifetime.lifetime * DURATION_FRACTION < lifetime.initial_lifetime {
            flickers
               .insert(
                  entity,
                  components::Flicker {
                     flicker_time: lifetime.lifetime,
                     original_color: colors.get(entity).cloned().unwrap_or(Rgba::WHITE),
                  },
               )
               .unwrap();
         }
      }
   }
}

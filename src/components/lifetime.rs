use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

pub struct Lifetime {
   pub lifetime: f32,
   pub initial_lifetime: f32,
}

impl Lifetime {
   pub fn new(lifetime: f32) -> Lifetime {
      Lifetime {
         lifetime,
         initial_lifetime: lifetime,
      }
   }
}

impl Component for Lifetime {
   type Storage = DenseVecStorage<Self>;
}

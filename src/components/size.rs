use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;
use crate::amethyst::renderer::Sprite;

pub struct Size {
   pub width: f32,
   pub height: f32,
}

impl Size {
   pub fn square(x: f32) -> Size {
      Size { width: x, height: x }
   }

   pub fn from_sprite(sprite: Sprite) -> Size {
      Size {
         width: sprite.width,
         height: sprite.height,
      }
   }
}

impl Component for Size {
   type Storage = DenseVecStorage<Self>;
}

package codes.brick.phaseshift.components;

import codes.brick.phaseshift.enums.Powerup;
import com.badlogic.ashley.core.Component;

public class PowerupComponent implements Component {
  public Powerup type;
}

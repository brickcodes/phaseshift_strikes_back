use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;
use std::collections::HashMap;

#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub enum Anim {
   Idle,
   Hurt,
   Shoot,
   AbilityOne,
   AbilityTwo,
}

pub struct Animation {
   // TODO: this could be a smallvec to avoid allocation/indirection,
   // though that cost is unlikely to show up on profiler
   pub anims: HashMap<Anim, &'static [usize]>,
   pub next_anim: Option<Anim>,
   pub cur_anim: Anim,
   pub cur_frame: usize,
}

impl Animation {
   pub fn with_anims(anims: HashMap<Anim, &'static [usize]>) -> Animation {
      Animation {
         anims,
         next_anim: None,
         cur_anim: Anim::Idle,
         cur_frame: 0,
      }
   }
}

impl Component for Animation {
   type Storage = DenseVecStorage<Self>;
}

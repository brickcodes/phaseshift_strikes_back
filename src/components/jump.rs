use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

use crate::JumpState;

pub const MAX_JUMP_SPEED: f32 = 1800.0;

pub struct Jump {
   pub jump_speed: f32,
   pub jump_state: JumpState,
   pub jump_triggered: bool,
}

impl Jump {}

impl Default for Jump {
   fn default() -> Jump {
      Jump {
         jump_speed: MAX_JUMP_SPEED,
         jump_state: JumpState::Standing,
         jump_triggered: false,
      }
   }
}

impl Component for Jump {
   type Storage = DenseVecStorage<Self>;
}

use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

#[derive(Clone, Debug, Default)]
pub struct Position {
   pub x: f32,
   pub y: f32,
   pub z: f32,
}

impl Component for Position {
   type Storage = DenseVecStorage<Self>;
}

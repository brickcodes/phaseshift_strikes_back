package codes.brick.phaseshift.components;

import codes.brick.phaseshift.enums.JumpState;
import com.badlogic.ashley.core.Component;

public class JumpComponent implements Component {
  public static final int MAX_JUMP_SPEED = 1800;
  public static final int JUMP_CHANGE = 7200;
  public float jumpSpeed = MAX_JUMP_SPEED;
  public JumpState jumpState = JumpState.STANDING;
  public boolean jumpTriggered = false;
}

use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;
use crate::amethyst::renderer::Rgba;

pub struct Flicker {
   pub flicker_time: f32,
   pub original_color: Rgba,
}

impl Component for Flicker {
   type Storage = DenseVecStorage<Self>;
}

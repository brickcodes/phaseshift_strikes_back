use crate::amethyst::core::specs::storage::NullStorage;
use crate::amethyst::core::specs::Component;

#[derive(Default)]
pub struct Fade;

impl Component for Fade {
   type Storage = NullStorage<Self>;
}

package codes.brick.phaseshift.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Color;

public class ColorComponent implements Component {
  public Color color = Color.WHITE;
  public Color initialColor = color;
}

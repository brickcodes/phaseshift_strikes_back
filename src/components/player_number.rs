use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

pub enum Number {
   One,
   Two,
}

pub struct PlayerNumber {
   pub number: Number,
}

impl Component for PlayerNumber {
   type Storage = DenseVecStorage<Self>;
}

use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

#[derive(Clone, Debug, Default)]
pub struct DeltaPosition {
   pub delta_x: f32,
   pub delta_y: f32,
}

impl Component for DeltaPosition {
   type Storage = DenseVecStorage<Self>;
}

use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::{Component, Entity};

pub struct Projectile {
   pub damage: i64,
   pub has_damaged: Vec<Entity>,
   pub owner: Entity,
}

impl Projectile {
   pub fn with_owner(owner: Entity) -> Projectile {
      Projectile {
         damage: 40,
         has_damaged: Vec::new(),
         owner,
      }
   }
}

impl Component for Projectile {
   type Storage = DenseVecStorage<Self>;
}

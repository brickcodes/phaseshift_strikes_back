use crate::amethyst::core::specs::storage::NullStorage;
use crate::amethyst::core::specs::Component;

#[derive(Default)]
pub struct Solid;

impl Component for Solid {
   type Storage = NullStorage<Self>;
}

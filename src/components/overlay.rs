use crate::amethyst::core::specs::storage::NullStorage;
use crate::amethyst::core::specs::Component;

#[derive(Default)]
pub struct Overlay;

impl Component for Overlay {
   type Storage = NullStorage<Self>;
}

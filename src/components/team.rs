use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

#[derive(Clone, PartialEq)]
pub struct Team {
   pub team: u8,
}

impl Component for Team {
   type Storage = DenseVecStorage<Self>;
}

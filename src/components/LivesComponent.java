package codes.brick.phaseshift.components;

import com.badlogic.ashley.core.Component;

public class LivesComponent implements Component {
  public int maxLives;
  public int lives;
}

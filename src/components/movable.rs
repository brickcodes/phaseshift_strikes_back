use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

use crate::Direction;

pub struct Movable {
   pub move_direction: Option<Direction>,
   pub horizontal_speed: f32,
}

impl Movable {
   pub fn with_default_horizontal_speed(move_direction: Option<Direction>) -> Movable {
      Movable {
         move_direction,
         horizontal_speed: 600.0,
      }
   }
}

impl Component for Movable {
   type Storage = DenseVecStorage<Self>;
}

package codes.brick.phaseshift.components;

import codes.brick.phaseshift.PowerupEffect;
import com.badlogic.ashley.core.Component;
import java.util.ArrayList;
import java.util.List;

public class PowerupEffectComponent implements Component {
  public List<PowerupEffect> activePowerupEffects = new ArrayList<PowerupEffect>();
}

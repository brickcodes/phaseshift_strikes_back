use crate::amethyst::core::specs::storage::NullStorage;
use crate::amethyst::core::specs::Component;

#[derive(Default)]
pub struct Dead;

impl Component for Dead {
   type Storage = NullStorage<Self>;
}

use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

pub struct Phaseshift {
   pub position: crate::components::Position,
   pub timer: f32,
}

impl Component for Phaseshift {
   type Storage = DenseVecStorage<Self>;
}

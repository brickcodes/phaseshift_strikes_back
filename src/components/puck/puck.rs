pub const IDLE_ANIM: [usize; 8] = [1, 1, 2, 2, 3, 3, 2, 2];

pub const ABILITY_ONE_ANIM: [usize; 9] = [1, 4, 5, 1, 6, 7, 6, 1, 5];

pub const HURT_ANIM: [usize; 1] = [0];

use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;
use crate::amethyst::ecs::Entity;

pub struct Puck {
   pub last_orb: Option<Entity>,
}

impl Component for Puck {
   type Storage = DenseVecStorage<Self>;
}

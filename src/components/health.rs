use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;
use std::cmp::min;

const MAX_HEALTH: i32 = 100;

pub struct Health {
   pub health: i32,
}

impl Default for Health {
   fn default() -> Health {
      Health { health: MAX_HEALTH }
   }
}

// Increase health by amount, with a max health of MAX_HEALTH.
impl Health {
   fn heal(&mut self, amount: i32) {
      self.health = min(self.health + amount, MAX_HEALTH);
   }
}

impl Component for Health {
   type Storage = DenseVecStorage<Self>;
}

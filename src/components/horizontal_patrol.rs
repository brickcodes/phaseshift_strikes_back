use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

use crate::Direction;

pub struct HorizontalPatrol {
   pub speed: f32,
   pub x_1: f32,
   pub x_2: f32,
   pub current_direction: Direction,
}

impl Component for HorizontalPatrol {
   type Storage = DenseVecStorage<Self>;
}

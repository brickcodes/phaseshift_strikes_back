package codes.brick.phaseshift.components;

import com.badlogic.ashley.core.Component;

public class FlipComponent implements Component {
  public boolean horizontalFlip;
  public boolean initialHorizontalFlip;

  public FlipComponent(boolean flip) {
    this.horizontalFlip = flip;
    this.initialHorizontalFlip = this.horizontalFlip;
  }
}

use crate::amethyst::core::specs::storage::DenseVecStorage;
use crate::amethyst::core::specs::Component;

pub struct Ability {
   pub ability_1_cooldown: f32,
   pub ability_1_triggered: bool,
   pub ability_2_cooldown: f32,
   pub ability_2_triggered: bool,
}

impl Default for Ability {
   fn default() -> Ability {
      Ability {
         ability_1_cooldown: 0.0,
         ability_1_triggered: false,
         ability_2_cooldown: 0.0,
         ability_2_triggered: false,
      }
   }
}

impl Component for Ability {
   type Storage = DenseVecStorage<Self>;
}

extern crate amethyst;

mod components;
mod systems;

use amethyst::assets::Processor;
use amethyst::assets::{AssetStorage, Handle, Loader, ProgressCounter};
use amethyst::audio::Source;
use amethyst::audio::SourceHandle;
use amethyst::audio::WavFormat;
use amethyst::core::nalgebra::Orthographic3;
use amethyst::core::specs::{Dispatcher, DispatcherBuilder, Entity};
use amethyst::core::timing::Time;
use amethyst::core::transform::{Transform, TransformBundle};
use amethyst::input::InputBundle;
use amethyst::prelude::*;
use amethyst::renderer::{
   Camera, ColorMask, DisplayConfig, DrawFlat2D, Flipped, Pipeline, PngFormat, Projection, RenderBundle, SpriteRender,
   SpriteSheet, SpriteSheetFormat, SpriteSheetHandle, Stage, Texture, TextureMetadata, Transparent, ALPHA,
};
use amethyst::ui::{Anchor, DrawUi, FontAsset, ScaleMode, TtfFormat, UiBundle, UiText, UiTransform};
use amethyst::utils::application_dir;
use amethyst::utils::fps_counter::{FPSCounter, FPSCounterBundle};
use amethyst::utils::ortho_camera::{CameraNormalizeMode, CameraOrtho, CameraOrthoSystem};
use rand::{FromEntropy, Rng};
use rand_xorshift::XorShiftRng;

use std::collections::HashMap;

pub type Rand = XorShiftRng;

pub const ARENA_WIDTH: f32 = 1024.0;
pub const ARENA_HEIGHT: f32 = 256.0;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Direction {
   Left,
   Right,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum JumpState {
   Standing,
   Jumping,
   Falling,
}

struct PreloadingState {
   loading_asset_progress: ProgressCounter,
   font: Option<Handle<FontAsset>>,
}

impl SimpleState for PreloadingState {
   fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
      let loader = data.world.read_resource::<Loader>();
      self.font = Some(loader.load(
         "font/PXSans/PXSansRegular.ttf",
         TtfFormat,
         (),
         &mut self.loading_asset_progress,
         &data.world.read_resource(),
      ));
   }

   fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
      data.data.update(&data.world);
      if self.loading_asset_progress.is_complete() {
         Trans::Switch(Box::new(LoadingState {
            game_asset_progress: ProgressCounter::new(),
            accumulated: 0.0,
            font: self.font.clone().unwrap(),
            loading_text: None,
            rand: XorShiftRng::from_entropy(),
            assets_opt: None,
         }))
      } else {
         Trans::None
      }
   }
}

fn load_sprite_sheet(world: &mut World, sheet_name: &str, prog_counter: &mut ProgressCounter) -> SpriteSheetHandle {
   let loader = world.read_resource::<Loader>();

   let texture_storage = world.read_resource::<AssetStorage<Texture>>();
   let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();

   let texture_handle = loader.load(
      format!("texture/{}.png", sheet_name),
      PngFormat,
      TextureMetadata::srgb_scale(),
      &mut *prog_counter,
      &texture_storage,
   );

   loader.load(
      format!("texture/{}.ron", sheet_name),
      SpriteSheetFormat,
      texture_handle,
      &mut *prog_counter,
      &sprite_sheet_store,
   )
}

/// Loads a wav audio track.
fn load_wav_track(world: &World, file: &str, prog_counter: &mut ProgressCounter) -> SourceHandle {
   let loader = world.read_resource::<Loader>();
   loader.load(
      file,
      WavFormat,
      (),
      prog_counter,
      &world.read_resource::<AssetStorage<Source>>(),
   )
}

type SoundAsset = (SourceHandle, f32);

#[derive(Clone)]
pub struct Assets {
   red_puck: SpriteSheetHandle,
   blue_puck: SpriteSheetHandle,
   red_orb: Handle<Texture>,
   blue_orb: Handle<Texture>,
   background: Handle<Texture>,
   white_pixel: Handle<Texture>,
   font: Handle<FontAsset>,
   phaseshift_sfx: SoundAsset,
   shoot_sfx: SoundAsset,
   jump_sfx: SoundAsset,
   teleport_sfx: SoundAsset,
}

pub struct LoadingState {
   game_asset_progress: ProgressCounter,
   accumulated: f32,
   font: Handle<FontAsset>,
   assets_opt: Option<Assets>,
   loading_text: Option<Entity>,
   rand: XorShiftRng,
}

impl SimpleState for LoadingState {
   fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
      let text = UiText::new(self.font.clone(), "Loading".into(), [1.0, 1.0, 1.0, 1.0], 128.0);
      // TODO: it is an unfortunate consequence that this means the loading text gets pushed around the screen,
      // I'd like to freeze the position of the text so that changing the text does not recalculate the position
      // not sure how to do this / if it is possible without putting in an inordinate amount of work
      let mut transform = UiTransform::new("loading_text".into(), Anchor::Middle, 0.0, 0.0, 1.0, 1.0, 1.0);
      transform.scale_mode = ScaleMode::Percent;

      let (blue_orb, red_orb, background, white_pixel) = {
         let loader = data.world.read_resource::<Loader>();
         (
            loader.load(
               "texture/blueorb.png",
               PngFormat,
               TextureMetadata::srgb_scale(),
               &mut self.game_asset_progress,
               &data.world.read_resource::<AssetStorage<Texture>>(),
            ),
            loader.load(
               "texture/redorb.png",
               PngFormat,
               TextureMetadata::srgb_scale(),
               &mut self.game_asset_progress,
               &data.world.read_resource::<AssetStorage<Texture>>(),
            ),
            loader.load(
               "texture/bg.png",
               PngFormat,
               TextureMetadata::srgb_scale(),
               &mut self.game_asset_progress,
               &data.world.read_resource::<AssetStorage<Texture>>(),
            ),
            loader.load(
               "texture/overlay.png",
               PngFormat,
               TextureMetadata::srgb_scale(),
               &mut self.game_asset_progress,
               &data.world.read_resource::<AssetStorage<Texture>>(),
            ),
         )
      };
      let blue_puck = load_sprite_sheet(data.world, "bluepuck", &mut self.game_asset_progress);
      let red_puck = load_sprite_sheet(data.world, "redpuck", &mut self.game_asset_progress);
      self.assets_opt = Some(Assets {
         blue_orb,
         red_orb,
         red_puck,
         blue_puck,
         background,
         white_pixel,
         font: self.font.clone(),
         shoot_sfx: (
            load_wav_track(&data.world, "sfx/shoot.wav", &mut self.game_asset_progress),
            1.0,
         ),
         jump_sfx: (
            load_wav_track(&data.world, "sfx/jump.wav", &mut self.game_asset_progress),
            0.8,
         ),
         teleport_sfx: (
            load_wav_track(&data.world, "sfx/teleport.wav", &mut self.game_asset_progress),
            1.0,
         ),
         phaseshift_sfx: (
            load_wav_track(&data.world, "sfx/phaseshift.wav", &mut self.game_asset_progress),
            1.0,
         ),
      });
      self.loading_text = Some(data.world.create_entity().with(text).with(transform).build());
   }

   fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
      data.data.update(&data.world);
      let mut ui_text_storage = data.world.write_storage::<UiText>();
      let ui_text = ui_text_storage.get_mut(self.loading_text.unwrap()).unwrap();
      self.accumulated += data.world.read_resource::<Time>().delta_seconds();
      while self.accumulated >= 0.2 {
         if ui_text.text.len() == 10 {
            ui_text.color = [self.rand.gen(), self.rand.gen(), self.rand.gen(), 1.0];
            ui_text.text.remove(ui_text.text.len() - 1);
            ui_text.text.remove(ui_text.text.len() - 1);
            ui_text.text.remove(ui_text.text.len() - 1);
         } else {
            ui_text.text.push('.');
         }
         self.accumulated -= 0.2;
      }
      if self.game_asset_progress.is_complete() {
         let assets = self.assets_opt.clone().unwrap();
         let game_dispatcher = DispatcherBuilder::new()
            .with(systems::EolFlicker, "eol_flicker", &[])
            .with(systems::Flicker::default(), "flicker", &["eol_flicker"])
            .with(systems::HorizontalPatrol, "horizontal_patrol", &[])
            .with(systems::KeyboardWatcher::default(), "keyboard", &[])
            .with(systems::Movement, "movement", &["keyboard"])
            .with(systems::Jump, "jump", &["keyboard"])
            .with(systems::PositionFinalize, "position_finalize", &["movement", "jump"])
            .with(CameraOrthoSystem::default(), "ortho_camera", &[])
            .with(systems::Lifetime, "lifetime", &[])
            .with(systems::Flip, "flip", &["keyboard"])
            .with(systems::Platform::default(), "platform", &[])
            .with(systems::Cooldown, "cooldown", &[])
            .with(systems::puck::Puck, "puck", &["keyboard", "cooldown"])
            .with(systems::puck::Phaseshift, "phaseshift", &[])
            .with(systems::Animation::default(), "animation", &["puck"])
            .with(systems::StaticCollision, "static_collision", &["position_finalize"])
            // should depend on all systems that update position or size
            .with(
               systems::SpriteTransform,
               "sprite_transform",
               &["horizontal_patrol", "puck", "position_finalize", "static_collision"],
            )
            .build();
         Trans::Switch(Box::new(RunningState {
            assets,
            game_dispatcher,
         }))
      } else {
         Trans::None
      }
   }
}

pub struct RunningState<'rs> {
   assets: Assets,
   game_dispatcher: Dispatcher<'rs, 'rs>,
}

impl<'rs> SimpleState for RunningState<'rs> {
   fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
      data.world.delete_all();
      data.world.add_resource(Rand::from_entropy());
      data.world.add_resource(self.assets.clone());
      data
         .world
         .add_resource(amethyst::audio::output::default_output().unwrap());
      self.game_dispatcher.setup(&mut data.world.res);
      // Camera
      {
         let mut camera_ortho = CameraOrtho::normalized(CameraNormalizeMode::Contain);
         camera_ortho.world_coordinates.right = ARENA_WIDTH;
         camera_ortho.world_coordinates.top = ARENA_HEIGHT;
         let mut transform = Transform::default();
         transform.set_z(11.0);
         data
            .world
            .create_entity()
            .with(Camera::from(Projection::Orthographic(Orthographic3::new(
               0.0,
               ARENA_WIDTH,
               0.0,
               ARENA_HEIGHT,
               10.0,
               0.0,
            ))))
            .with(transform)
            .with(camera_ortho)
            .build();
      }
      // Background
      {
         let position = components::Position { x: 0.0, y: 0.0, z: 1.0 };
         let size = components::Size {
            width: ARENA_WIDTH,
            height: ARENA_HEIGHT,
         };
         data
            .world
            .create_entity()
            .with(position)
            .with(size)
            .with(self.assets.background.clone())
            .build();
      }
      // Add Pucks
      {
         create_puck_entity(
            data.world,
            components::player_number::Number::One,
            self.assets.blue_puck.clone(),
            0,
         );
         create_puck_entity(
            data.world,
            components::player_number::Number::Two,
            self.assets.red_puck.clone(),
            1,
         );
      }
   }

   fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
      self.game_dispatcher.dispatch(&data.world.res);
      data.data.update(&data.world);
      println!("{}", data.world.res.fetch::<FPSCounter>().sampled_fps());
      Trans::None
   }
}

fn create_puck_entity(
   world: &mut World,
   player_number: components::player_number::Number,
   puck_sheet: SpriteSheetHandle,
   team_num: u8,
) {
   use crate::components::animation::Anim;
   use crate::components::puck::puck::{ABILITY_ONE_ANIM, HURT_ANIM, IDLE_ANIM};

   let mut animations: HashMap<Anim, &'static [usize]> = HashMap::with_capacity(3);
   animations.insert(Anim::Idle, &IDLE_ANIM);
   animations.insert(Anim::Hurt, &HURT_ANIM);
   animations.insert(Anim::AbilityOne, &ABILITY_ONE_ANIM);
   world
      .create_entity()
      .with(components::Position { x: 0.0, y: 0.0, z: 5.0 })
      .with(components::DeltaPosition::default())
      .with(components::Size::square(64.0))
      .with(SpriteRender {
         sprite_sheet: puck_sheet,
         sprite_number: 0,
      })
      .with(components::PlayerNumber { number: player_number })
      .with(components::Movable::with_default_horizontal_speed(None))
      .with(components::Jump::default())
      .with(components::Ability::default())
      .with(components::Health::default())
      .with(components::Team { team: team_num })
      .with(components::puck::Puck { last_orb: None })
      .with(components::Animation::with_anims(animations))
      .with(Flipped::Horizontal)
      .with(Transparent)
      .build();
}

fn main() -> amethyst::Result<()> {
   //amethyst::start_logger(Default::default());

   let path = application_dir("resources/display_config.ron")?;
   let config = DisplayConfig::load(&path);

   let pipe = Pipeline::build().with_stage(
      Stage::with_backbuffer()
         .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
         .with_pass(DrawFlat2D::new().with_transparency(ColorMask::all(), ALPHA, None))
         .with_pass(DrawUi::new()),
   );

   let keyboard_binding_path = "resources/keyboard_bindings_config.ron";

   let input_bundle = InputBundle::<String, String>::new().with_bindings_from_file(keyboard_binding_path)?;

   let game_data = GameDataBuilder::default()
      .with_bundle(
         RenderBundle::new(pipe, Some(config))
            .with_sprite_sheet_processor()
            .with_sprite_visibility_sorting(&[]),
      )?
      .with_bundle(TransformBundle::new())?
      .with_bundle(input_bundle)?
      .with(Processor::<Source>::new(), "audio_processor", &[])
      .with_bundle(UiBundle::<String, String>::new())?
      .with_bundle(FPSCounterBundle::default())?;

   let mut game = Application::new(
      "./",
      PreloadingState {
         loading_asset_progress: ProgressCounter::new(),
         font: None,
      },
      game_data,
   )?;
   game.run();

   Ok(())
}
